import React, { Component } from 'react';
import './App.css';
import MainLayout from './layout/main.jsx'

class App extends Component {
  render() {
    return (
      <div className="App" >
        <MainLayout />
      </div>
    );
  }
}

export default App;
