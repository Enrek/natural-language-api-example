import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import InputData from '../component/input';
import Result from '../component/result';
import axios from 'axios';
import CircularProgress from '@material-ui/core/CircularProgress';
import  cyan from '@material-ui/core/colors/cyan';

const styles = theme => ({
  root: {
    flexGrow: 1,
    margin: 1 + "em",
    height: '100%'
  },
  progress: {
    margin: theme.spacing.unit * 2,
    height: '2em'
  }
});

class MainLayout extends Component {

  constructor(props) {
    super(props);
    this.state = {
      value: '',
      entities: [],
      loading: false,
      result: false,
      santiment: 0
    };
  }

  handleSubmit = (event) => {
    event.preventDefault();
    if (!this.state.value || this.state.loading)
      return;

    var data = JSON.stringify(
      {
        "document": {
          "type": "PLAIN_TEXT",
          "content": this.state.value
        },
        "encodingType": "UTF8",
        "features": {
          "extractEntities": true,
          "extractDocumentSentiment": true,
          "extractEntitySentiment": true
        }
      }
    )

    axios.request({
      method: "post",
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      url: "https://language.googleapis.com/v1beta2/documents:annotateText?key=CHANGE_IT",
      data: data,
      onUploadProgress: (p) => {
        this.setState({ loading: true });
      }
    }).then(response => {
      this.setState({ loading: false });
      this.saveEnties(response.data)
      this.setState({ result: true });
    });
  }

  saveEnties(data) {
    this.setState({ santiment: data.documentSentiment.score }); 
    var entities = [];
    data.entities.forEach(entity => {
      var note = {
        title: entity.name,
        type: entity.type,
        sentiment: entity.sentiment
      };
      if (entity.metadata && entity.metadata.wikipedia_url) {
        note = {
          ...note,
          wiki: entity.metadata.wikipedia_url
        }
      }
      entities.push(note)
    });
    this.setState({ entities: entities });
  }

  handleChange = (event) => {
    this.setState({ value: event.target.value });
  }

  getBody = (props) => {
    if (this.state.loading)
      return (
        < Grid
          container  >
          <Grid item xs={12}>
            <CircularProgress className={props.classes.progress} style={{ color: cyan[300], height: '15em', width: '15em' }} thickness={7} />
          </Grid>
        </ Grid>
      )
    else if (this.state.result)
      return (
        <Grid container spacing={8}>
          <Grid item xs={12}>
            <InputData handleChange={this.handleChange}
              handleSubmit={this.handleSubmit}
              value={this.state.value} />
          </Grid>

          <Grid item xs={12}>
            {this.state.entities.length !== 0 && <Result santiment={this.state.santiment} entities={this.state.entities} />}
          </Grid>
        </Grid>
      )
    else
      return (
        <Grid container
          direction="row"
          justify="center"
          alignItems="center" spacing={8}>
          <Grid item xs={12}>
            <InputData handleChange={this.handleChange}
              handleSubmit={this.handleSubmit}
              value={this.state.value} />
          </Grid>
        </Grid>
      )
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        {this.getBody(this.props)}
      </div>
    );
  }
}

MainLayout.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MainLayout);
