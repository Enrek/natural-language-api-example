import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import Typography from '@material-ui/core/Typography';

const styles = {
  root: {
    flexGrow: 1,
  },
  colorPrimary: {
    backgroundColor: '#B2DFDB',
  },
  barColorPrimary: {
    backgroundColor: '#00695C',
  },
  colorPrimaryGrey: {
    backgroundColor: '#6f6f6f',
  },
  barColorPrimaryGrey: {
    backgroundColor: '#6f6f6f',
  },
  colorPrimaryGreen: {
    backgroundColor: '#618833',    
  },
  barColorPrimaryGreen: {
    backgroundColor: '#8bc34a',
  },
  text: {
    marginTop: 0.2 + "em"
  }
};

class LinearDeterminate extends React.Component {


  mapRange(value, low1, high1, low2, high2) {
    return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        {this.props.score == 0 &&
          <LinearProgress variant="determinate"
            value={this.mapRange(this.props.score, 0, 1, 50, 100)}
            classes={{ colorPrimary: classes.colorPrimaryGrey, barColorPrimary: classes.barColorPrimaryGrey }}
          />
        }
        {this.props.score > 0 &&
          <LinearProgress variant="determinate"
            value={this.mapRange(this.props.score, 0, 1, 50, 100)}
            classes={{ colorPrimary: classes.colorPrimaryGreen, barColorPrimary: classes.barColorPrimaryGreen }}
          />
        }
        {this.props.score < 0 &&
          <LinearProgress variant="determinate"
            color="secondary"
            value={this.mapRange(this.props.score, -1, 0, 0, 50)}
          />
        }

        {(this.props.magnitude || this.props.magnitude === 0) &&
          <Typography className={classes.text} color="textSecondary">
            Sentiment: {this.props.score} ({this.props.magnitude})
          </Typography>
        }
      </div>
    );
  }
}

LinearDeterminate.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LinearDeterminate);
