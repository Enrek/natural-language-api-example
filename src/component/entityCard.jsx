import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Sentiment from './sentiment';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';

const styles = theme => ({
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    sentiment: {
        marginTop: 0.2 + "em",
        textAlign: 'left'
    }
});

class EntityCard extends React.Component {

    render() {
        const { classes } = this.props;
        return (
            <Paper className={classes.paper}>
                <Typography variant="h2" >
                    {this.props.title}
                </Typography>
                <Typography variant="h5" gutterBottom color="primary" className={classes.button}>
                    {this.props.type}
                </Typography>
                <Sentiment className={classes.sentiment} score={this.props.sentiment.score} magnitude={this.props.sentiment.magnitude}></Sentiment>
            </Paper>
        );
    }
}

EntityCard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(EntityCard);
