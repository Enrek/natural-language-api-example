import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  root: {
    flexGrow: 1,
    margin: 1 + "em"
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  title: {
    color: "white",
    fontWeight: 300,
    letterSpacing: "0.4em",
    margin: "0.9em"
  },
  textInput: {
    width: '100%',
    border: 'none',
    height: '100px',
    outline: 'none'
  },
  subbmitButton: {
    backgroundColor: '#303f9f',
    width: '100%',
    height: '100%',
    fontSize: '2.3rem',
    fontWeight: 200,
    marginRight: theme.spacing.unit * 2,

  },
});

class InputData extends Component {


  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Grid container spacing={8}>
          <Grid item xs={12}>
            <Typography className={classes.title} variant="h2" >
              INPUT
            </Typography>
          </Grid>

          <Grid item xs={11}>
            <Paper className={classes.paper} >
              <form noValidate autoComplete="off">
                <textarea
                  value={this.props.value}
                  onChange={this.props.handleChange}
                  className={classes.textInput}
                  placeholder="Enter your text"
                  component="textarea"
                />
              </form>
            </Paper>
          </Grid>
          <Grid item xs={1}>
            <Button variant="contained" onClick={this.props.handleSubmit} color="primary" className={classes.subbmitButton}>
              Send
          </Button>
          </Grid>
        </Grid>
      </div>
    );
  }
}

InputData.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(InputData);
