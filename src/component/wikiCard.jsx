import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core';
import PropTypes from 'prop-types';
import React, { Component } from 'react';

const styles = {
    card: {
        maxWidth: 100 + "%",
    },
    text: {
        textAlign: "left"
    },
    media: {
        // ⚠️ object-fit is not supported by IE11.
        objectFit: 'cover',
    },
};

class WikiCard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null,
            image: null,
            url: null,
            title: null
        };
    }

    componentDidMount() {
        if (!this.props.title)
            return;
            fetch('https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro&explaintext&redirects=1&titles=' + this.props.title)
                .then(response => response.json())
                .then(data => {
                    this.setState({ data: data.query.pages[Object.keys(data.query.pages)[0]].extract })
                });
            fetch('https://en.wikipedia.org/w/api.php?action=query&prop=pageimages&format=json&pithumbsize=450&titles=' + this.props.title)
                .then(response => response.json())
                .then(data => {
                    if(data.query.pages[Object.keys(data.query.pages)[0]].thumbnail)
                        this.setState({ image: data.query.pages[Object.keys(data.query.pages)[0]].thumbnail.source })
                    if(!this.state.image) {
                            fetch('https://en.wikipedia.org/w/api.php?action=query&prop=pageimages&format=json&titles=' + this.props.title)
                            .then(response => response.json())  
                            .then(data => {
                                if(data.query.pages[Object.keys(data.query.pages)[0]].thumbnail)
                                    this.setState({ image: data.query.pages[Object.keys(data.query.pages)[0]].thumbnail.source })
                            });
                        }
                });
            
            fetch('https://en.wikipedia.org/w/api.php?action=query&prop=info&inprop=url&format=json&titles=' + this.props.title)
                .then(response => response.json())
                .then(data => {
                    this.setState({
                        url: data.query.pages[Object.keys(data.query.pages)[0]].fullurl,
                        title: data.query.pages[Object.keys(data.query.pages)[0]].title
                    })
                });
        
    }

    truncate(string, max) {
        if (string.length > max)
            return string.substring(0, max) + '...';
        else
            return string;
    };


    render() {
        const { classes } = this.props;
        return (

            <Card className={classes.card}>
                <CardActionArea>
                    {this.state.image &&
                        <CardMedia
                            component="img"
                            className={classes.media}
                            height="450"
                            image={this.state.image}
                            title="Contemplative Reptile"
                        />}
                    <CardContent>
                        <Typography gutterBottom variant="h4" component="h2">
                            {this.state.title && this.state.title}
                        </Typography>
                        <Typography className={classes.text} component="p">
                            {this.state.data && this.truncate(this.state.data, 600)}
                        </Typography>
                    </CardContent>
                </CardActionArea>
                {this.state.url &&
                    <CardActions>
                        <a href={this.state.url} target="_blank" >
                            <Button size="small" color="primary">
                                Learn More
                    </Button>
                        </a>
                    </CardActions>
                }
            </Card>
        );
    }
}

WikiCard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(WikiCard);