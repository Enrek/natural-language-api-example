import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import WikiCard from '../component/wikiCard';
import EntityCard from '../component/entityCard'
import Typography from '@material-ui/core/Typography';
import Sentiment from './sentiment';
import Paper from '@material-ui/core/Paper';


const styles = theme => ({
    root: {
        flexGrow: 1,
        margin: 1 + "em"
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    text: {
        textAlign: "left"
    },
    title: {
        color: "white",
        fontWeight: 300,
        letterSpacing: "0.4em",
        margin: "0.9em"
    },
    textInput: {
        width: '100%',
        border: 'none',
        height: '100px',
        outline: 'none'
    },
    subbmitButton: {
        backgroundColor: '#303f9f',
        width: '100%',
        height: '100%',
        fontSize: '2.7em',
        fontWeight: 200,
        marginRight: theme.spacing.unit * 2,

    },
    santimentTotal: {
        textAlign: 'left',
        marginBottom: '0.3em'
    }
});

class Result extends Component {

    render() {
        const { classes, entities } = this.props;

        return (
            <div className={classes.root}>
                <Grid container spacing={8}>

                    <Grid item xs={12}>
                        <Typography className={classes.title} variant="h2" >
                            RESULT
                        </Typography>
                    </Grid>

                    <Grid item xs={4}>
                        <Grid container spacing={8}>
                            <Grid item xs={12}>
                                <Paper className={classes.paper}>
                                    <Typography className={classes.santimentTotal} variant="h4" >
                                        Total santiment: {this.props.santiment}
                                    </Typography>
                                    <Sentiment score={this.props.santiment} />
                                </Paper>
                            </Grid>
                            {entities.map((entity, i) => {
                                return (
                                    <Grid key={i + "-entity"} item xs={12}>
                                        <EntityCard title={entity.title} type={entity.type} sentiment={entity.sentiment} />
                                    </Grid>
                                )
                            })}
                        </Grid>
                    </Grid>
                    <Grid item xs={8}>
                        <Grid container spacing={8}>
                            {entities.map((entity, i) => {
                                return (entity.wiki &&
                                    <Grid key={i + "-wiki"} item xs={6}>
                                        <WikiCard title={entity.wiki.slice(entity.wiki.indexOf('wiki/') + 5)} />
                                    </Grid>
                                )
                            })}
                        </Grid>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

Result.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Result);
